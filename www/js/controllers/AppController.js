var main_module = angular.module('cars_app.controllers', []);

main_module.controller('AppCtrl', ["$scope", "$ionicModal", "$timeout", "$ionicActionSheet", "registrationservice", "$localstorage", "$state", "$ionicSlideBoxDelegate", function($scope, $ionicModal, $timeout, $ionicActionSheet, registrationservice, $localstorage, $state, $ionicSlideBoxDelegate) {
    //Sign in function...       
    $scope.login = {
        username: "",
        password: "",
        status: ""
    };

    $scope.user = {
        firstname: '',
        lastname: '',
        company: '',
        companycode: '',
        email: '',
        confirmemail: '',
        password: '',
        confirmpassword: '',
        mobile: '',
        phone: '',
        gender: '',
        dateofbirth: ''
    };
    $scope.allimages = [];

    $scope.signin_action = function() {
        registrationservice.login($scope.login.username, $scope.login.password).success(function(data) {
            console.log(data);
            if (data.success == true) {
                $localstorage.set('username', data.data.User.username);
                $localstorage.set('id', data.data.User.id);
                $localstorage.set('token', data.data.User.token);
                $scope.login.status = "Login success";
                $state.go("app.carsell");
            } else {
                $scope.login.status = "Incorrect username or password, Try again.";
            }
        });
    }

   

    $scope.registration_action = function() {
        var _user = $scope.user;
        var _data = {
            'data[User][0][first_name]': _user.firstname,
            'data[User][0][last_name]': _user.lastname,
            'data[User][0][username]': _user.email,
            'data[User][0][confirm_email]': _user.confirmemail,
            'data[User][0][password]': _user.password,
            'data[User][0][confirm_password]': _user.confirmpassword,
            'data[Member][member_type]': 1,
            'data[Member][mobile]': _user.mobile,
            'data[Member][company_name]': _user.company,
            'data[Member][code]': _user.companycode,
            'data[Member][phone]': _user.phone,
            'data[User][0][gender]': _user.gender,
            'data[User][0][dob]': _user.dateofbirth
        };
        registrationservice.registration(_data, $scope);
    }

    $scope.showDatePicker = function($event) {
        var options = {
            date: new Date(),
            mode: 'date'
        };
        datePicker.show(options, function(date) {
            if (date != 'Invalid Date') {
                alert(date);
                //$scope.user.dateofbirth= "";
                //var datef = date.format("dd-m-yy");
                $scope.user.dateofbirth = date;
            } else {
                console.log(date);
            }
        });
        $event.stopPropagation();
    };


    $scope.testService = function() {
        var userid = $localstorage.get('id', '');
        var token = $localstorage.get('token', '');
        registrationservice.testservice(userid, token).success(function(data) {
            console.log(data);
        });
    }

    //Show options menu when click on header right icon...
    $scope.showOptions = function() {
        $ionicActionSheet.show({
            buttons: [{
                text: 'Setting'
            }, {
                text: 'Dashboard'
            }, {
                text: 'Share'
            }],
            buttonClicked: function(index) {
                alert(index);
                return true;
            }
        });
    }

}]);
main_module.controller('SearchController', ["$scope", "SearchService", "Utilities", "$state", function($scope, SearchService, Utilities, $state) {
    $scope.landing = {
        quicksearch: ""
    };
    $scope.search_result = "";
    $scope.search = function() {
        $scope.search_result = "";
        var _data = {};
        if (!angular.isUndefined($scope.landing.quicksearch)) {
            _data = Utilities._param({
                "keysearch": $scope.landing.quicksearch
            });
        }
        SearchService.search_action(_data).success(function(_response) {
            $scope.search_result = _response.data;
            var search_data = angular.toJson($scope.search_result);
            $state.go("app.searchcars", {
                'search_result': search_data
            });
        });
    }
}]);

main_module.controller('SearchResult', ['$scope', '$stateParams', function($scope, $stateParams) {
    $scope.search_result = angular.fromJson($stateParams
        .search_result);
}]);

main_module.controller('CarDetails', ['$scope', '$stateParams', 'carDetailsService', '$localstorage', function($scope, $stateParams, carDetailsService, $localstorage) {
    var carid = $stateParams.carid;
    $scope.cardata = "";
    carDetailsService.car_details(carid).success(function(_response) {
        $scope.cardata = _response;
        $scope.allslide = _response.data.current.Car.images;
    });

    var userid = $localstorage.get('id', '');
    var token = $localstorage.get('token', '');
    if (token != '') {
        carDetailsService.recentview(userid, token, carid).success(function(data) {
            console.log(data);
        });
    } else {
        alert("Login first to listed this car");
    }

}]);
