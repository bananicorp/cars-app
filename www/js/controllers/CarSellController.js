main_module.controller('CarSell', ['$scope', '$rootScope', 'carSellService', 'registrationservice', 'FilterService', '$localstorage', 'appconfig', 'Camera', '$upload', function($scope, $rootScope, carSellService, registrationservice, FilterService, $localstorage, appconfig, Camera, $upload) {
    $scope.loading = "";
    $scope.sellstep = 1;
    $scope.is_login = 1;
    //$scope.islogedin = (parseInt(LogEdIn) == 0 ? false : true);
    $scope.selllogin = {
        username: '',
        password: ''
    };
    $scope.months = [01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12];
    $scope.search = {
        id: 0,
        first_name: '',
        last_name: '',
        user_password: '',
        user_repassword: '',
        user_email: '',
        user_confirmemail: '',
        make: '',
        model: '',
        year: '',
        variant: '',
        postcode: '',
        suburb: '',
        condition: '',
        vin_code: '',
        odometer: '',
        color: '',
        registered: '',
        price: '',
        allow_offers: '',
        ad_email: '',
        show_email: '',
        ad_phone: '',
        show_phone: '',
        vehicle: '',
        notes: '',
        main_image: '',
        images: [],
        standard_features: [],
        optional_features: [],
        rego: '',
        rego_exp_month: '',
        rego_exp_year: '',
        uploaded_image: '',
        uploaded_images: [],
        croped_image: '',
        file: '',
        status: '',
    };
    $scope.cars = {};
    $scope.progress = {
        width: '0%',
        count: 0
    };
    $scope.standart_features = [];
    $scope.optional_features = [];
    $scope.smodulas = [];
    $scope.omodulas = [];
    $scope.featured = {
        sfeatures: [],
        ofeatures: []
    };
    $scope.selectedimageforcroped = '';
    $scope.search.imagepreviewinadd = '';
    $scope.postcodes = [];
    $scope.colorLists = [];
    $scope.selectedColor = "";

    $scope.search = {
        condition: '',
        make: '',
        model: '',
        min_price: '',
        max_price: '',
        location: '',
        transmission: '',
        keywords: '',
        body: ''
    };

    $scope.login = {
        username: "",
        password: ""
    };

    $scope.imageloc = "";

    var _search = $scope.search;
    $scope.init = function() {
        carSellService.getMakes().success(function(_response) {
            _search.makes = _response.data;
        });

        /*     if (angular.isUndefined($rootScope.filter)) {
                 FilterService.get_filter().success(function(data) {
                     $scope.search.makes = data.data.Default.Makes;
                     $scope.search.bodys = data.data.Car.Body.options;
                     $scope.search.locations = data.data.Car.Location.options;
                     $scope.search.conditions = data.data.Car.Condition.options;
                     $scope.search.transmissions = data.data.Car.Transmission.options;
                     console.log($scope.search.conditions);
                 });
             }  */

        $scope.$watch('search.make', function(n, o) {
            if (n != o && n != null) {
                _search.make = n;
                _search.model = null;
                _search.year = null;
                _search.variant = null;
                carSellService.getSourceOptions(_search).success(function(_response) {
                    $scope.search.models = _response.data;
                });
            }
        });

        $scope.$watch('search.model', function(n, o) {
            if (n != o && n != null) {
                _search.model = n;
                _search.year = null;
                _search.variant = null;
                carSellService.getSourceOptions(_search).success(function(_response) {
                    $scope.search.years = _response.data;
                });
            }
        });

        $scope.$watch('search.year', function(n, o) {
            if (n != o && n != null) {
                _search.year = n;
                _search.variant = null;
                carSellService.getSourceOptions(_search).success(function(_response) {
                    $scope.search.variants = _response.data;
                });
            }
        });

        $scope.$watch('search.color', function(n, o) {
            $scope.colorLists = [];
            if (n != o && n.length > 1 && $scope.selectedColor != n) {
                carSellService.getColor(n).success(function(data) {
                    angular.forEach(data.colorList, function(value, key) {
                        if (value != null && angular.isString(value)) {
                            $scope.colorLists.push(value)
                        }
                        if (angular.isArray($scope.colorLists) && $scope.colorLists.length > 0) {
                            $scope.drop1_style = {
                                'display': 'block'
                            };
                        }
                    })
                })
            }
        });


        $scope.$watch('search.postcode', function(n, o) {
            $scope.postcodes = [];
            if (n != o && n.length > 3) {
                registrationservice.postcode(n).success(function(data) {
                    angular.forEach(data, function(value, key) {
                        if (value != null && angular.isObject(value.Postcode)) {
                            $scope.postcodes.push(value.Postcode);
                        }
                    });
                    if (angular.isArray($scope.postcodes) && $scope.postcodes.length > 0) {
                        $scope.drop_style = {
                            'display': 'block'
                        };
                    }
                    console.log($scope.postcodes);
                });
            } else {
                $scope.drop_style = {
                    'display': 'none'
                };
            }
        });
    }
    $scope.init();


    $scope.findcar = function() {
        //$localstorage.set('username', '');
        if (_search.make == "" || _search.model == "" || _search.year == "") {
            alert("Please select make, model and year to search");
        } else {
            carSellService.getCars(_search).success(function(_response) {
                console.log(_response);
                $scope.glasscars = _response.data;
                $scope.cars_total = $scope.glasscars.total;
                delete $scope.glasscars.total;
                console.log($scope.glasscars);
                angular.forEach($scope.glasscars, function(value, index) {
                    $scope.glasscars[index].Source.imgurl = "http://dev.crinitis.net.au/co/v2/images/gdb/" + $scope.glasscars[index].Source.nvic + ".jpg";
                    //$scope.progress.width = 20 + "%";
                    //$scope.progress.count = 20;
                });
                $scope.loading = "";
            });
        }
    }


    $scope.glassCarSelect = function(_nvice, _img) {
        console.log(_nvice);
        //$scope.loading = 'active';
        //$scope.search.imagepreviewinadd = _img;
        $scope.standart_features = [];
        $scope.featured = {
            sfeatures: "",
            ofeatures: ""
        };
        $scope.featured.sfeatures = [];
        $scope.search.standard_features = [];
        $scope.search.optional_features = [];
        carSellService.getCar(_nvice).success(function(data) {
            if (data.success == true) {
                $scope.search.bt = data.data.Source.bt;
                $scope.search.cc = data.data.Source.cc;
                $scope.search.cyl = data.data.Source.cyl;
                $scope.search.engine = data.data.Source.engine;
                $scope.search.et = data.data.Source.et;
                $scope.search.make = data.data.Source.make;
                $scope.search.model = data.data.Source.model;
                $scope.search.nvic = data.data.Source.nvic;
                $scope.search.series = data.data.Source.series;
                $scope.search.size = data.data.Source.size;
                $scope.search.style = data.data.Source.style;
                $scope.search.transmission = data.data.Source.transmission;
                $scope.search.tt = data.data.Source.tt;
                $scope.search.variant = data.data.Source.variant;
                $scope.search.width = data.data.Source.width;
                $scope.search.year = data.data.Source.year;
                $scope.search.vehicle = data.data.Source.style + " " + data.data.Source.transmission;

                carSellService.getFeatures(_nvice).success(function(fdata) {
                    if (fdata.data.Standard != null) {
                        var _sfeatures = fdata.data.Standard.split(',');
                        angular.forEach(_sfeatures, function(value, key) {
                            var _feature = value.split(':');
                            $scope.standart_features.push({
                                val: _feature[0],
                                text: _feature[1]
                            });
                            $scope.featured.sfeatures.push(_feature[1]);
                        })
                        $scope.search.standard_features = $scope.featured.sfeatures;
                        $scope.standartFeatureInit();
                    }

                    if (fdata.data.Optional != null) {
                        var _ofeatures = fdata.data.Optional.split(',');
                        angular.forEach(_ofeatures, function(value, key) {
                            var _feature = value.split(':');
                            $scope.optional_features.push({
                                val: _feature[0],
                                text: _feature[1]
                            });
                            $scope.featured.ofeatures.push(_feature[1]);
                        })
                        $scope.search.optional_features = $scope.featured.ofeatures;
                        $scope.optionalFeatureInit();
                    }
                    //$scope.next(2);
                    //$scope.loading = '';

                });

            }
        });
        $scope.sellstep = 2;
    }

    $scope.standartFeatureInit = function() {
        while ($scope.standart_features.length) {
            $scope.smodulas.push($scope.standart_features.splice(0, 5));
        }
    }
    $scope.optionalFeatureInit = function() {
        while ($scope.optional_features.length) {
            $scope.omodulas.push($scope.optional_features.splice(0, 5))
        }
    }

    $scope.next = function(_step) {
        console.log("Step ", _step);
        if (_step == 1) {
            // $scope.progress.count = 20
            $scope.sellstep = _step;
        } else if (_step == 2) {
            //$scope.progress.count = 40
            $scope.sellstep = _step;
        } else if (_step == 3) {
            //$scope.progress.count = 60
            $scope.sellstep = _step;
        } else if (_step == 4) {
            var saveusername = $localstorage.get('username', '');
            console.log("username ", $localstorage.get('username', ''));
            if (saveusername != '') {
                _step = 5;
            }
            $scope.sellstep = _step;
            // $scope.progress.count = 80
        } else if (_step == 5) {
            var saveusername = $localstorage.get('username', '');
            console.log('Username', $scope.selllogin.username);
            if (saveusername == '') {
                registrationservice.login($scope.selllogin.username, $scope.selllogin.password).success(function(data) {
                    console.log(data);
                    if (data.success == true) {
                        $localstorage.set('username', data.data.User.username);
                        console.log("username ", $localstorage.get('username', ''));
                        alert("Login success..");
                        $scope.sellstep = _step;
                    } else {
                        alert("Incorrect username or password!");
                        _step = 4;
                        $scope.sellstep = _step;
                    }
                });
            } else {
                $scope.sellstep = _step;
            }
            //$scope.progress.count = 100 
        } else if (_step == 6) {
            $scope.sellstep = _step;
        }
        //$scope.progress.width = $scope.progress.count + "%";

        //$scope.scrollFix();

        //console.log($scope.search);
    }

    /*   $scope.posteCodeBlur = function() {
           if ($scope.postcodes.length > 0) {
               $scope.drop_style = {
                   'display': 'block'
               };
           }
       } */

    $scope.postCodeSelect = function(item) {
        $scope.search.suburb = item.suburb;
    }

    $scope.ColorSelect = function(item) {
        // $scope.selectedColor = item;
        $scope.search.color = item;
    }

    $scope.getCameraPhoto = function() {
        Camera.getPicture().then(function(imageData) {
            alert(imageData);
            /*var img_b64 = canvas.toDataURL(imageData);
            var imagefile = img_b64.split(',')[1];
            var file_object = new Blob([window.atob(imagefile)], {
                type: 'image/png',
                encoding: 'utf-8'
            }); */
            // var imgfile = new Image();
            //imgfile.src = imageData;
            //var file_object = 
            //alert(img);
            //var imgfile = "data:image/jpeg;base64," + imageData;
            //$scope.imageloc = imgfile;;
            var userid = $localstorage.get('id', '');
            var token = $localstorage.get('token', '');
            carSellService.cameraPhotoUpload(userid, token, imageData).success(function(data) {
                console.log(data);
                alert(data);
            });
        }, function(err) {
            console.err(err);
        });
    }


    $scope.search.uploaded_images = [];
    $scope.search.images = [];

    function carimageupload($files) {
        var userid = $localstorage.get('id', '');
        var token = $localstorage.get('token', '');
        var _file = $files;
        $scope.search.file = _file;
        $scope.upload = $upload.upload({
            url: appconfig.api.post_carsimageupload,
            data: {
                "data[User][id]": userid,
                "data[User][token]": token,
                "data[mobile]": true,
                "data[Photo][photos][]": $files,
                "data[Photo][model]": 'Car'
            },
            method: 'POST',
            file: $scope.search.file
        }).success(function(data) {
            if (data.success == true) {
                $scope.search.uploaded_image = data.data.url;
                $scope.selectedimageforcroped = data.name;
                $scope.search.uploaded_images.push({
                    src: data.data.url,
                    name: data.name
                });
                $scope.search.images.push(data.name);
                $scope.search.imagepreviewinadd = data.data.url;
                $scope.search.main_image = data.name;
            }
        });
    }

    $scope.fileUpload = function($files) {
        var userid = $localstorage.get('id', '');
        var token = $localstorage.get('token', '');
        var _file = $files[0];
        console.log("upload file", $files[0].toString());
        $scope.search.file = _file;
        $scope.upload = $upload.upload({
            url: appconfig.api.post_carsimageupload,
            data: {
                "data[User][id]": userid,
                "data[User][token]": token,
                "data[mobile]": true,
                "data[Photo][photos][]": $files[0],
                "data[Photo][model]": 'Car'
            },
            method: 'POST',
            file: $scope.search.file
        }).success(function(data) {
            if (data.success == true) {
                $scope.search.uploaded_image = data.data.url;
                $scope.selectedimageforcroped = data.name;
                $scope.search.uploaded_images.push({
                    src: data.data.url,
                    name: data.name
                });
                $scope.search.images.push(data.name);
                $scope.search.imagepreviewinadd = data.data.url;
                $scope.search.main_image = data.name;
            }
        });
    }


    $scope.saveCar = function(_step) {
        console.log(_step);
        var _data = $scope.search;
        //_data.standard_features=_data.standard_features.toString();
        _data.standard_features = $scope.featured.sfeatures.toString();
        _data.optional_features = $scope.featured.ofeatures.toString();
        _data.user_email = $scope.search.user_email;
        _data.user_confirmemail = $scope.search.user_confirmemail;
        _data.user_password = $scope.search.user_password;
        _data.user_repassword = $scope.search.user_repassword;

        _data.userid = $localstorage.get('id', '');
        _data.token = $localstorage.get('token', '');
        // _data.images = _data.images.toString();
        // _data.status=_status;
        //console.log(_data);
        //console.log($scope.objToString($scope.toObject(_data.images)));
        carSellService.saveCarsData(_data).success(function(data) {
            if (data.success == false) {
                alert("Save car error, try again!");
            } else {
                $scope.search.id = data.data.Car.id;
                if (_step == 1) {
                    $scope.sellstep = 6;
                } else {
                    $scope.sellstep = 7;
                }
            }
        });
    }




}]);
