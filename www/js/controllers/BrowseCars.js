main_module.controller('BrowseCars', ['$scope', '$http', '$compile', 'carDetailsService', 'appconfig', function($scope, $http, $compile, carDetailsService, appconfig) {

    carDetailsService.getFilters().success(function(data) {
        i = 0;
        $scope.groups = [];
        $scope.makes = [];
        options = [];
        $scope.filter = data.data.Filter;
        //console.log($scope.filter);
        firstgroup = data.data.Filter.make;
        firstgroup.group = "makes";

        angular.forEach(data.data.Default.Makes, function(index, item) {
            $scope.makes[i] = {
                "value": item,
                "selected": ""
            };
            i++;
        });

        firstgroup.options = $scope.makes;
        firstgroup.open = true;
        $scope.groups[0] = firstgroup;


        angular.forEach($scope.filter, function(index, item) {
            if (item.selected) {
                angular.forEach(item.selected, function(i, v) {
                    var a = $scope.groups[0].options.objIndexOf(v);
                    if (!angular.isUndefined($scope.groups[0].options[a])) {
                        $scope.groups[0].options[a].selected = true;
                    }
                });
            }
        });

        i = 1;

        angular.forEach(data.data.Car, function(item, index) {
            $scope.groups[i] = item;
            //console.log($scope.groups[i]);
            i++;
        });

        angular.forEach($scope.groups, function(item, index) {
            if (index > 0) {
                if (item.selected) {
                    angular.forEach(item.selected, function(i, v) {
                        angular.forEach($scope.groups[index].options, function(i, option) {
                            options[i] = {
                                "label": option.label,
                                "value": option.label,
                                "selected": ""
                            };
                        });
                        var a = options.objIndexOf(v);
                        console.log(a + "-" + $scope.groups[index].options[a]);
                        if (!angular.isUndefined($scope.groups[index].options[a])) {
                            $scope.groups[index].options[a].selected = true;
                        }
                    });
                }
            }
        });
    });

    $scope.tmpl_compile = function(string, tag, val, mval) {
        tag = '{' + tag + '}';
        var re = new RegExp(/\{([^}]+)\}/);
        while (match = re.exec(string)) {
            if (match[0].indexOf('v') > 0) {
                string = string.replace(match[0], mval);
            } else {
                var _sval = val.toString().toLowerCase();
                _sval = _sval.charAt(0).toUpperCase() + _sval.slice(1);
                string = string.replace(match[0], _sval);
            }
        }
        return string;
    }

    $scope.getModels = function(item) {
        $scope.loaded = [];
        var make = item.make.value;
        datapost = "data[Filter][make][]=" + make;
        //console.log(item);

        checked = $("#" + make.replace(' ', '_')).prop('checked');
        $scope.loaded[make.replace(' ', '_')] = $("#" + make.replace(' ', '_')).attr("data-load");

        //console.log("index>" + $scope.loaded[make.replace(' ', '_')]);
        //console.log(checked);
        if (checked === false) {
            $('#branch_' + make.replace(' ', '_')).hide();
        } else {
            $("#" + make.replace(' ', '_')).attr("data-load", true);
            $('#branch_' + make.replace(' ', '_')).show();

        }
        console.log(make + "D");
        if ($scope.loaded[make.replace(' ', '_')] === undefined) {
            $http({
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                url: appconfig.api.post_getfilteroptions,
                method: "POST",
                data: datapost
            }).success(function(data) {
                $scope.models = data;
                console.log(data);
                var branch = $('#branch_' + make.replace(' ', '_'));
                branch.load('templates/branchModels.html', function() {
                    tpl = $('#branch_' + make.replace(' ', '_') + ' ul').html();
                    models = '';
                    angular.forEach($scope.models, function(v, i) {
                        if (v !== null && angular.isObject(v) == false) {
                            var mv = make + ":" + v;
                            models += $scope.tmpl_compile(tpl, 'model', v, mv);
                        }
                    });
                    $('#branch_' + make.replace(' ', '_') + ' ul').html(models);
                });
                var linkFn = $compile(branch.contents());
                element = linkFn($scope);
                branch.html(element);
            });
        }
    }

    $scope.goClick = function() {
        $('select[name="data[Car][radius]"]').remove();
        var _form = $("#CarSearchForm").serialize();
        console.log(_form);
        //window.location.href = appconfig.baseUrl + 'cars/#!/search.html/mainsearch/' + _form + ".html";
    }


    $scope.clearFilters = function() {
        $('#CarSearchForm')[0].reset();
    }




    /* 
     * if given group is the selected group, deselect it
     * else, select the given group
     */
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };

}]);
