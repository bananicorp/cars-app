cars_app.factory('carSellService', ['$http', 'appconfig', 'Utilities', function($http, appconfig, Utilities) {
    return {
        getMakes: function() {
            return $http({
                url: appconfig.api.post_getMakes,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        getSourceOptions: function(_data) {
            return $http({
                url: appconfig.api.post_getSourceOptions,
                method: 'POST',
                data: Utilities._param({
                    'data[Filter][make]': _data.make,
                    'data[Filter][model]': _data.model,
                    'data[Filter][year]': _data.year
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        getCars: function(_data) {
            return $http({
                url: appconfig.api.post_getSourceCars,
                method: 'POST',
                data: Utilities._param({
                    'data[Filter][make]': _data.make,
                    'data[Filter][model]': _data.model,
                    'data[Filter][year]': _data.year,
                    'data[Filter][variant]': _data.variant
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        getCar: function(_nvice) {
            return $http({
                url: appconfig.api.get_getSourceCar + '/' + _nvice,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        getFeatures: function(_nvice) {
            return $http({
                url: appconfig.api.post_getSourceFeatures,
                method: 'POST',
                data: Utilities._param({
                    'data[Source][nvic]': _nvice
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        getColor: function(_data) {
            return $http({
                url: appconfig.api.post_getColor,
                data: Utilities._param({
                    'data[color]': _data
                }),
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },
        saveCarsData: function(data) {
            return $http({
                url: appconfig.api.post_carAdd,
                method: 'POST',
                data: Utilities._param({
                    'data[User][id]': data.userid,
                    'data[User][token]': data.token,
                    'data[mobile]': true,
                    'data[User][0][first_name]': data.first_name,
                    'data[User][0][last_name]': data.last_name,
                    'data[User][0][username]': data.user_email,
                    'data[User][0][confirm_email]': data.user_confirmemail,
                    'data[User][0][password]': data.user_password,
                    'data[User][0][confirm_password]': data.user_repassword,
                    'data[Member][member_type]': 1,
                    'data[Member][mobile]': data.ad_phone,
                    'data[Car][id]                   ': data.id,
                    'data[Car][first_name]                   ': data.first_name,
                    'data[Car][last_name]                    ': data.last_name,
                    'data[Car][ad_email]                     ': data.ad_email,
                    'data[Car][ad_phone]                     ': data.ad_phone,
                    'data[Car][vehicle]                      ': data.vehicle,
                    'data[Car][make]                         ': data.make,
                    'data[Car][model]                        ': data.model,
                    'data[Car][year]                         ': data.year,
                    'data[Car][variant]                      ': data.variant,
                    'data[Car][series]                       ': data.series,
                    'data[Car][optional_features]            ': data.optional_features,
                    'data[Car][standard_features][]            ': data.standard_features,
                    'data[Car][postcode]                     ': data.postcode,
                    'data[Car][suburb]                       ': data.suburb,
                    'data[Car][condition]                    ': data.condition,
                    'data[Car][vin_code]                     ': data.vin_code,
                    'data[Car][odometer]                     ': data.odometer,
                    'data[Car][price]                        ': data.price,
                    'data[Car][rego]                         ': data.rego,
                    'data[Car][rego_exp_month]                         ': data.rego_exp_month,
                    'data[Car][rego_exp_year]                         ': data.rego_exp_year,
                    'data[Car][color]                        ': data.color,
                    'data[Car][registered]                   ': data.registered,
                    'data[Car][allow_offers]                 ': data.allow_offers,
                    'data[Car][show_email]                   ': data.show_email,
                    'data[Car][show_phone]                   ': data.show_phone,
                    'data[Car][main_image]                   ': data.main_image,
                    'data[Car][images]                       ': data.images,
                    'data[Car][description]                  ': data.description,
                    'data[Car][notes]                        ': data.notes,
                    'data[Car][status]                        ': data.status,
                    'data[Car][nvic]                         ': data.nvic
                }, true),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },

        cameraPhotoUpload: function(userid, token, imageData) {
            return $http({
                url: appconfig.api.post_cameraimgupload,
                data: Utilities._param({
                    "data[User][id]": userid,
                    "data[User][token]": token,
                    "data[mobile]": true,
                    "data[imagedata]": imageData
                }),
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },


    }
}]);
