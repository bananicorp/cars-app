cars_app.factory('SearchService', ['$http', 'appconfig', function($http, appconfig) {
    return {
        search_action: function(_data) {
            return $http({
                url: appconfig.api.post_search,
                method: "POST",
                data: _data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

        }
    }
}]);
