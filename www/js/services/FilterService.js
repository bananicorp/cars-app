cars_app.factory('FilterService', ['$http', '$rootScope', 'appconfig', 'Utilities', function($http, $rootScope, appconfig, Utilities) {
    var _makes = {};
    return {
        get_filter: function() {
            return $http({
                url: appconfig.api.get_getfilter,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                appconfig.filter(data);
            });
        },
        get_filteroptions: function(_make) {
            return $http({
                url: appconfig.api.post_getfilteroptions,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: Utilities._param({
                    'data[Filter][make][]': _make
                }),
            }).success(function(data) {

            });
        },
        makes: function() {
            return _makes;
        }

    }

}]);
