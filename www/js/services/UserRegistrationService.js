cars_app.factory('registrationservice', ['$http', 'Utilities', 'appconfig', function($http, Utilities, appconfig) {
    return {
        login: function(username, password) {
            return $http({
                url: appconfig.api.login,
                method: 'POST',
                data: Utilities._param({
                    'data[User][username]': username,
                    'data[User][password]': password
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },

        testservice: function(id, token) {
            return $http({
                url: appconfig.api.get_testUrl,
                method: 'POST',
                data: Utilities._param({
                    'data[User][id]': data.userid,
                    'data[User][token]': data.token,
                    'data[mobile]': true,
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },

        postcode: function(_val) {
            return $http({
                url: appconfig.api.post_postCode,
                method: 'POST',
                data: Utilities._param({
                    'postcode': _val,
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        },

        registration: function(_data, _scope) {
            $http({
                url: appconfig.api.post_registration,
                method: 'POST',
                data: Utilities._param(_data),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(data) {
                console.log(data);
                if (data.success == true) {
                    alert("Registration Success");
                    _scope.login_name = _scope.user.first_name;

                } else {
                    alert("Registration Failed");
                    console.log(data.error.User);
                }
            });
        }
    }
}]);
