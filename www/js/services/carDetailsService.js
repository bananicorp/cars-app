cars_app.factory('carDetailsService', ['$http', 'appconfig', 'Utilities', function($http, appconfig, Utilities) {

    return {
        car_details: function(_id) {
            return $http({
                url: appconfig.api.get_car_details + _id,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },

        recentview: function(userid, token, carid) {
            return $http({
                url: appconfig.api.post_recentview,
                method: 'POST',
                data: Utilities._param({
                    'data[User][id]': userid,
                    'data[User][token]': token,
                    'data[mobile]': true,
                    'data[MemberAction][recent_views]': carid
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        },

        getFilters: function() {
            return $http({
                url: appconfig.api.get_getfilter,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            });
        }


    }

}]);
