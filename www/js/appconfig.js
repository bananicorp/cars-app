cars_app.factory('appconfig', ['$rootScope', function($rootScope) {
    var serviceUrl = "http://uniquecarsoffer.com.au/services/";
    var baseUrl = "http://uniquecarsoffer.com.au/";
    var localUrl = "http://carsoffer.loc/services/";
    return {
        api: {
            login: serviceUrl + "users/login/",
            post_search: serviceUrl + "cars/getCars/",
            get_car_details: serviceUrl + "cars/getcar/",
            post_getMakes: serviceUrl + "Sources/getMakes/",
            post_getSourceOptions: serviceUrl + "Sources/getSourceOptions/",
            post_getSourceCars: serviceUrl + "Sources/getCars/",
            get_getSourceCar: serviceUrl + "Sources/getCar/",
            post_getSourceFeatures: serviceUrl + "Sources/getFeatures/",
            post_postCode: serviceUrl + "cars/getSuburbs/",
            get_getfilter: serviceUrl + "cars/getFilters/",
            post_getfilteroptions: serviceUrl + "cars/getFilterOptions/",
            post_getColor: serviceUrl + "cars/getColorsSuggestionList/",
            post_carsimageupload: serviceUrl + "cars/upload/",
            post_carAdd: serviceUrl + "cars/save/",
            post_registration: serviceUrl + "members/register/",
            post_recentview: serviceUrl + "members/saveAction/",
            post_cameraimgupload: serviceUrl + "cars/cameraimgupload/",
            get_testUrl: serviceUrl + "users/test"
        },
        filter: function(_data) {
            if (angular.isUndefined($rootScope.filter)) {
                $rootScope.filter = _data;
            }
        }
    }
}]);
