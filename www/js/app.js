// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var cars_app = angular.module('cars_app', ['ionic', 'cars_app.controllers', 'angularFileUpload'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $stateProvider

        .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })

    .state('app.profile', {
        url: "/profile",
        views: {
            'menuContent': {
                templateUrl: "templates/profile.html"
            }
        }
    })

    .state('app.home', {
        url: "/home",
        views: {
            'menuContent': {
                templateUrl: "templates/home.html"
            }
        }
    })


    .state('app.dashboard', {
        url: "/dashboard",
        views: {
            'menuContent': {
                templateUrl: "templates/dashboard.html"
            }
        }
    })


    .state('app.setting', {
        url: "/setting",
        views: {
            'menuContent': {
                templateUrl: "templates/setting.html"
            }
        }
    })


    .state('app.about', {
        url: "/about",
        views: {
            'menuContent': {
                templateUrl: "templates/about.html"
            }
        }
    })

    .state('app.signin', {
        url: "/signin",
        views: {
            'menuContent': {
                templateUrl: "templates/signin.html"
            }
        }
    })

    .state('app.signup', {
        url: "/signup",
        views: {
            'menuContent': {
                templateUrl: "templates/signup.html"
            }
        }
    })

    .state('app.searchcars', {
        url: "/searchcars?search_result",
        views: {
            'menuContent': {
                templateUrl: "templates/searchcars.html",
                controller: "SearchResult"
            }
        }
    })

    .state('app.cardetails', {
        url: "/cardetails/:carid",
        views: {
            'menuContent': {
                templateUrl: "templates/cardetails.html",
                controller: "CarDetails"
            }
        }
    })

    .state('app.carsell', {
        url: "/carsell",
        views: {
            'menuContent': {
                templateUrl: "templates/carsell.html",
                controller: "CarSell"
            }
        }
    })

    .state('app.browsecars', {
        url: "/browsecars",
        views: {
            'menuContent': {
                templateUrl: "templates/browsecars.html",
                controller: "BrowseCars"
            }
        }
    })

    .state('app.landing', {
        url: "/landing",
        views: {
            'menuContent': {
                templateUrl: "templates/landing.html",
                controller: "SearchController"
            }
        }
    });

    $urlRouterProvider.otherwise('/app/landing');
    $ionicConfigProvider.views.maxCache(0);
});
